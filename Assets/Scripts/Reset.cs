﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reset : MonoBehaviour
{
    [SerializeField]
    private GameObject pauseMenu;

    void Start()
    {
        StartCoroutine(Stargame());
    }
    
    private IEnumerator Stargame()
    {
        while (true)
        {
            if (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0))
            {
                SceneManager.LoadScene(0);
                pauseMenu.SetActive(false);
                Time.timeScale = 1;
            }
            yield return null;
        }
    }
}
