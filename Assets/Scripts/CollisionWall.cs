﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionWall : MonoBehaviour
{

    public event Action<Obstacle> onCheckObstacle;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out Obstacle obstacle))
        {
            onCheckObstacle?.Invoke(obstacle);
        }
    }


}
