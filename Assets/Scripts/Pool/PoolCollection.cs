﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolCollection : MonoBehaviour
{
    [SerializeField]
    private int sizePool;

    [SerializeField]
    private Transform parent;

    [SerializeField]
    private Obstacle platform;

    [SerializeField]
    private GameObject pauseMenu;

    public List<Obstacle> SetupPool()
    {
        List<Obstacle> obstacles = new List<Obstacle>();
        for (int i = 0; i < sizePool; i++)
        {
            Obstacle obj = Instantiate(platform, parent);
            obj.PauseMenu(pauseMenu);
            obstacles.Add(obj);
            obj.gameObject.SetActive(false);
        }
        return obstacles;
    }
}
