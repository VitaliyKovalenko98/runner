﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolMover : MonoBehaviour
{
    [SerializeField]
    private PoolCollection whitePlatformPool;

    [SerializeField]
    private PoolCollection redPlatformPool;

    [SerializeField]
    private CollisionWall collisionWall;

    [SerializeField]
    private Transform startDownPoint;

    [SerializeField]
    private Transform startUpPoint;

    [SerializeField]
    private float addRate;


    private List<Obstacle> redObstacles;
    private List<Obstacle> whiteObstacles;
    private List<Obstacle> obstaclesActive = new List<Obstacle>();


    private void Awake()
    {
        redObstacles = redPlatformPool.SetupPool();
        whiteObstacles = whitePlatformPool.SetupPool();
        collisionWall.onCheckObstacle += FadeObstacle;
    }

    private void Start()
    {
        
        StartCoroutine(SpawnObstacles());
    }

    private void FadeObstacle(Obstacle obs)
    {
        obs.StopMove();
        obs.gameObject.SetActive(false);
        if (obs.IsRed)
            redObstacles.Add(obs);
        else
            whiteObstacles.Add(obs);
    }

    private void PlaceObstacles(Obstacle downObs, Obstacle upObs)
    {
        PlaceObstacle(downObs, startDownPoint);
        PlaceObstacle(upObs, startUpPoint);
    }

    private void PlaceObstacle(Obstacle obs, Transform startPoint)
    {
        obstaclesActive.Add(obs);
        obs.gameObject.SetActive(true);
        obs.transform.position = startPoint.position;
        obs.StartMove();
    }


    private Obstacle RemoveObstacle(List<Obstacle> obstacles)
    {
        Obstacle obs = obstacles[obstacles.Count - 1];
        obstacles.RemoveAt(obstacles.Count - 1);
        return obs;
    }

    private IEnumerator SpawnObstacles()
    {
        int obstacleCount = 0;
        bool isRed = false;

        while (true)
        {
            if (obstacleCount == 0)
            {
                obstacleCount = Random.Range(1, 3);
                isRed = (Random.Range(0, 2) == 0 ? true : false);
            }
            if (isRed)
            {
                PlaceObstacles(RemoveObstacle(redObstacles), RemoveObstacle(whiteObstacles));
            }
            else
            {
                PlaceObstacles(RemoveObstacle(whiteObstacles), RemoveObstacle(redObstacles));
            }


            obstacleCount--;
            yield return new WaitForSeconds(addRate);
        }
    }
}
