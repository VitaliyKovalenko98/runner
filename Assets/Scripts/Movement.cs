﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField]
    private float jumpForce;

    [SerializeField]
    private LayerMask groundMask;

    private bool isGrounded;
    private Rigidbody2D rb;
    private CircleCollider2D playerColider;

    private void Awake()
    {
        playerColider = GetComponent<CircleCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        StartCoroutine(Jump());
    }
    private IEnumerator Jump()
    {
        while (true)
        {
            isGrounded = Physics2D.IsTouchingLayers(playerColider, groundMask);
            if (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0))
            {
                if (rb.gravityScale == 1 && isGrounded)
                {
                    rb.velocity = new Vector2(0, jumpForce);
                }
                else if(rb.gravityScale == 1 && !isGrounded)
                {
                    rb.gravityScale = -1;
                }
                else if(rb.gravityScale == -1 && isGrounded)
                {
                    rb.gravityScale = 1;
                    yield return new WaitForSeconds(1.1f);
                }
            }
            
            yield return null;
        }
    }
}
