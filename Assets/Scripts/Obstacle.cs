﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField]
    private bool isRed;

    [SerializeField]
    private float speed;

    [SerializeField]
    private GameObject pauseMenu;

    private Transform startPosition;

    public bool IsRed => isRed;
        

    public void StartMove()
    {
        StartCoroutine(MoveForvard());
    }

    public void SaveState(bool isRed)
    {
        this.isRed = isRed;
    }

    public void PauseMenu(GameObject pauseMenu)
    {
        this.pauseMenu = pauseMenu;
    }

    public void StopMove()
    {
        StopCoroutine(MoveForvard());
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isRed && collision.gameObject.tag == "Player")
        {
            pauseMenu.SetActive(true);
            Time.timeScale = 0;
        }
    }

    private IEnumerator MoveForvard()
    {
        while (true)
        {
            Vector2 newPos = new Vector2(transform.position.x - 1, transform.position.y);
            transform.position = Vector2.MoveTowards(transform.position, newPos, speed);
            yield return new WaitForFixedUpdate();
        }
    }
}
