﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scores : MonoBehaviour
{
    [SerializeField]
    public Text scoresText;

    private float scores = 0;

    public void Start()
    {
        scoresText.text = scores.ToString();
        StartCoroutine(ScoresChange());
    }

    private IEnumerator ScoresChange()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            scores += 1;
            scoresText.text = scores.ToString();

        }
    }
}
