﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPlatformsMove : MonoBehaviour
{
    [SerializeField]
    private float speed;

    void Start()
    {
        StartCoroutine(MoveForvard());
    }
    private IEnumerator MoveForvard()
    {
        while (true)
        {
            Vector2 newPos = new Vector2(transform.position.x - 1, transform.position.y);
            transform.position = Vector2.MoveTowards(transform.position, newPos, speed);
            yield return new WaitForFixedUpdate();
        }
    }
}
